//http://api.openweathermap.org/data/2.5/forecast?q=mumbai&units=metric&APPID=08f23eafec81300c7b6bf742f4b299fb

const key ="08f23eafec81300c7b6bf742f4b299fb";

const getForecast = async (city) => {
	const base = "http://api.openweathermap.org/data/2.5/forecast";
	const query= `?q=${city}&units=metric&APPID=${key}`;

const response = await fetch(base+query);
	//console.log(response);
	
	if(response.ok){
		const data = await response.json();
		return data;
	}else{
		throw new Error("Error Status" + response.status);
	}
}


	getForecast("Mumbai")
	.then(data => console.log(data))
.catch(err => console.log(err));

